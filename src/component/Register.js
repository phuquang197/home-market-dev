import React, { useEffect, useReducer, useState } from 'react';
import axios from "axios";
function Register() {
    const[register, setRegister] =useState('')

    const[dataApi, setDataApi] =useState('')
    
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnb29nbGVJRCI6IjExMzA0MjkyMjU4OTkyODA1MjI3OCIsInJlc3VsdCI6eyJzdG9yZU93bmVySUQiOnsibmFtZSI6bnVsbCwicGhvdG9zIjpudWxsLCJlbWFpbCI6bnVsbCwiZGVzY3JpcHRpb24iOm51bGwsInBob25lTnVtYmVycyI6bnVsbCwiYWRkcmVzcyI6bnVsbCwic3RhdHVzIjoiTk9TVEFUVVMiLCJfaWQiOiI2MDc1YWFjNTQ3ZWZhYzAwM2JiMGUxNDUiLCJjcmVhdGVkQXQiOiIyMDIxLTA0LTEzVDE0OjI5OjI1LjUyN1oiLCJ1cGRhdGVkQXQiOiIyMDIxLTA0LTEzVDE0OjI5OjI1LjUyN1oiLCJfX3YiOjB9LCJlbXBsb3llZUlEIjpudWxsLCJwcm9maWxlUGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdnYmFpejFXMlFrVmxOd3J3OTVVN3poVWVtNGdqcVV4NlZnRjkzaWNRPXM5Ni1jIiwiZ2VuZGVyIjpudWxsLCJmdWxsTmFtZSI6IlF1YW5nIHVuZGVmaW5lZCIsImVtYWlsIjoicGh1cXVhbmcxOTdAZ21haWwuY29tIiwicGhvbmUiOm51bGwsImZhY2Vib29rSUQiOm51bGwsImdvb2dsZUlEIjoiMTEzMDQyOTIyNTg5OTI4MDUyMjc4Iiwic3RhdHVzIjoiVU5MT0NLIiwicm9sZVBlbmRpbmciOiJCQVNJQyIsInJvbGUiOiJCQVNJQyIsIl9pZCI6IjYwNzVhYWM1NDdlZmFjMDAzYmIwZTE0NiIsImNyZWF0ZWRBdCI6IjIwMjEtMDQtMTNUMTQ6Mjk6MjUuNTUyWiIsInVwZGF0ZWRBdCI6IjIwMjEtMDQtMTNUMTQ6Mjk6MjUuNTUyWiIsIl9fdiI6MH0sImlhdCI6MTYxODMyNDE2NSwiZXhwIjoxNjMzODc2MTY1fQ.5GDecqUleJ5p_2ZcKivo8GDpx139kw_wT2Zp0Sql1Ss'
    const fetchData =  async() => {
         await axios({
        method: 'post',
        url: 'http://homemarket.us-3.evennode.com/register',
        data: {
         ...register,
        },
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
    }
    function submit(){
      fetchData()
      console.log(register);
    }
    console.log(dataApi);
    return (
        <div>
            <div style={{ color: '#747487' }} className="container form">
                <div className="row">
                    <div style={{ color: '#fff' }} className="col-md-6 imglogin pt-5">
                        <div className="text-center pt-5">
                            <h1 className="login-title">ĐĂNG NHẬP</h1>
                            <h4 className="login-title mb-5">Chào mừng trở lại!</h4>
                            <h6 className="pt-5">Bạn mới sử dụng Home Martket? <a href="login.html">Đăng kí miễn phí</a>
                            </h6>
                        </div>
                    </div>
                    <div className="col-md-6 pt-5 pr-5 pl-5">
                        <form className="text-left">
                            <div className="mb-3">
                                <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                                <input  type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                onChange={event => setRegister({...register,userName:event.target.value})}
                                />
                                <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1"
                                 onChange={event => setRegister({...register,passWord:event.target.value})}
                                />
                            </div>
                            <div className="mb-3 form-check">
                                <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                            </div>
                            <button type="submit" className="btn btn-primary w-100 mb-5">Submit</button>
                        </form>
                        <form className="signwith pb-4">
                            <div className="row">
                                <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                                <div className="col-md-4 text-center"><small>Hoặc đăng nhập bằng</small></div>
                                <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                            </div>
                            <div className="d-grid gap-2 d-md-block text-center">
                                <a className="btn fb" href="#" role="button">
                                    <div className="icon_fb icon2">
                                        <i className="fa fa-facebook" aria-hidden="true" />
                                    </div> <br />
                                    <h6>Facebook</h6>
                                </a>
                                <a className="btn gm" href="#" role="button">
                                    <div className="icongm icon2">
                                        <i className="fa fa-google" aria-hidden="true" />
                                    </div><br />
                                    <h6>Gmail</h6>
                                </a>
                            </div>
                            <small className="pdt-md pt-4" id="agree-terms">
                                <font style={{ verticalAlign: 'inherit' }}>
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        Bằng cách đăng nhập, tôi đồng ý với </font>
                                </font><a className="rule" target="_blank" href="/privacy">
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}>Chính sách Bảo mật</font>
                                    </font>
                                </a>
                                <font style={{ verticalAlign: 'inherit' }}>
                                    <font style={{ verticalAlign: 'inherit' }}> và </font>
                                </font><a className="rule" target="_blank" href="/privacy">
                                    <font style={{ verticalAlign: 'inherit' }}>
                                        <font style={{ verticalAlign: 'inherit' }}>Điều Khoản Dịch Vụ</font>
                                    </font>
                                </a>
                                <font style={{ verticalAlign: 'inherit' }}>
                                    <font style={{ verticalAlign: 'inherit' }}> .</font>
                                </font>
                            </small>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Register
