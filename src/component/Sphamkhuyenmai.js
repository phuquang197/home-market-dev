import React, { Component } from 'react'
export default function Sphamkhuyenmai () {    
        return (
            <div>
                <section style={{ margin: '0 auto', position: 'relative', height: '763px' }} className="spkhuyenmai mt-4">
                    <div className="bgimg">
                        <h1 className="text-center pt-5">SẢN PHẨM KHUYẾN MÃI</h1>
                        <div className="middle">
                            <p className="p-5 mb-3" id="timer" />
                        </div>
                        <div className="bottomleft">
                        </div>
                    </div>
                    <div className="container mt-5 spkm">
                        <h4 style={{ fontFamily: '"Oswald", sans-serif', fontWeight: 'bold' }} className="mb-4 pt-4 text-center">
                        </h4>
                        <div className="row mt-5">
                            <div className="col-md-3 col-sm-6">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-left ml-2">
                                            <small className="sieuthi mb-3">Lotel mart</small>
                                            <p className="card-title tensp">Dâu Tây</p>
                                            <small className="trongluong">300g</small>
                                            <p className="gia mt-4">159,000đ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-6">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div className="giamgia">
                                            <span className="stick-note p-1">30%</span>
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-center">
                                            <h4 className="card-title">Title</h4>
                                            <div className="rating">
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                            </div>
                                            <p className="card-text">Text</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-6">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div className="giamgia">
                                            <span className="stick-note p-1">30%</span>
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-center">
                                            <h4 className="card-title">Title</h4>
                                            <div className="rating">
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                            </div>
                                            <p className="card-text">Text</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-6">
                                <div className="card">
                                    <div className="khoi">
                                        <div className="layer1">
                                            <img className="card-img-top" src="https://baokhuyennong.com/wp-content/uploads/2020/03/ca-loc-1.png" alt="" />
                                        </div>
                                        <div className="giamgia">
                                            <span className="stick-note p-1">30%</span>
                                        </div>
                                        <div style={{ width: '100%' }} className="layer2">
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                            <div className="icon-card">
                                                <i className="fa fa-cart-plus p-2" aria-hidden="true" />
                                            </div>
                                        </div>
                                        <div className="card-body text-center">
                                            <h4 className="card-title">Title</h4>
                                            <div className="rating">
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                                <i className="fa fa-star-o mb-3" aria-hidden="true" />
                                            </div>
                                            <p className="card-text">Text</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="text-center mb-4">
                            <button type="button" className="btn btn-outline-success">Xem thêm</button>
                        </div>
                    </div>
                </section>

            </div>
        )
    }

