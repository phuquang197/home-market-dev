import React, { Component, useState } from 'react';
export default function Menu () {
        return (
        <div>
        <section className="menu">
          <div className="container-fluid">
            <nav style={{ backgroundColor: 'rgb(0 0 0 / 50%)' }} id="menu" className="navbar navbar-expand-lg navbar-dark text-center pt-4 pb-4">
              <a className="navbar-brand" href="#">Home Market</a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span style={{ color: 'white' }} className="navbar-toggler-icon" />
              </button>
              <div className="collapse navbar-collapse pr-3" id="navbarText">
                <ul className="navbar-nav mr-auto justify-content-center">
                  <li className="nav-item active">
                    <a className="nav-link" href="#">Trang Chủ
            </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">Giới Thiệu</a>
                  </li>
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Ngành hàng
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a className="dropdown-item" href="#">Action</a>
                      <a className="dropdown-item" href="#">Another action</a>
                      <div className="dropdown-divider" />
                      <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                  </li>
                  <li className="nav-item ml-2 mr-2">
                    <a className="nav-link" href="#">Khuyến Mãi</a>
                  </li>
                </ul>
                <span className="navbar-text pr-3">
                  <button style={{ border: 'none' }} className="btnlogin p-2" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Đăng
                    Nhập</button>
                  <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog text-center">
                      <div style={{ width: '890px' }} className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title" id="exampleModalLabel">Sign in</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div style={{ color: '#747487' }} className="container form">
                          <div className="row">
                            <div style={{ color: '#fff' }} className="col-md-6 imglogin pt-5">
                              <div className="text-center pt-5">
                                <h1 className="login-title">ĐĂNG NHẬP</h1>
                                <h4 className="login-title mb-5">Chào mừng trở lại!</h4>
                                <h6 className="pt-5">Bạn mới sử dụng Home Martket? <a href="login.html">Đăng kí miễn phí</a>
                                </h6>
                              </div>
                            </div>
                            <div className="col-md-6 pt-5 pr-5 pl-5">
                              <form className="text-left">
                                <div className="mb-3">
                                  <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                                  <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" 
                                   />
                                  <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                                </div>
                                <div className="mb-3">
                                  <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                  <input type="password" className="form-control" id="exampleInputPassword1" />
                                </div>
                                <div className="mb-3 form-check">
                                  <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                  <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                                </div>
                                <button type="submit" className="btn btn-primary w-100 mb-5">Submit</button>
                              </form>
                              <form className="signwith pb-4">
                                <div className="row">
                                  <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                                  <div className="col-md-4 text-center"><small>Hoặc đăng nhập bằng</small></div>
                                  <div className="col-md-4" style={{ borderBottom: 'solid 0.5px silver' }} />
                                </div>
                                <div className="d-grid gap-2 d-md-block text-center">
                                  <a className="btn fb" href="#" role="button">
                                    <div className="icon_fb icon2">
                                      <i className="fa fa-facebook" aria-hidden="true" />
                                    </div> <br />
                                    <h6>Facebook</h6>
                                  </a>
                                  <a className="btn gm" href="#" role="button">
                                    <div className="icongm icon2">
                                      <i className="fa fa-google" aria-hidden="true" />
                                    </div><br />
                                    <h6>Gmail</h6>
                                  </a>
                                </div>
                                <small className="pdt-md pt-4" id="agree-terms">
                                  <font style={{ verticalAlign: 'inherit' }}>
                                    <font style={{ verticalAlign: 'inherit' }}>
                                      Bằng cách đăng nhập, tôi đồng ý với </font>
                                  </font><a className="rule" target="_blank" href="/privacy">
                                    <font style={{ verticalAlign: 'inherit' }}>
                                      <font style={{ verticalAlign: 'inherit' }}>Chính sách Bảo mật</font>
                                    </font>
                                  </a>
                                  <font style={{ verticalAlign: 'inherit' }}>
                                    <font style={{ verticalAlign: 'inherit' }}> và </font>
                                  </font><a className="rule" target="_blank" href="/privacy">
                                    <font style={{ verticalAlign: 'inherit' }}>
                                      <font style={{ verticalAlign: 'inherit' }}>Điều Khoản Dịch Vụ</font>
                                    </font>
                                  </a>
                                  <font style={{ verticalAlign: 'inherit' }}>
                                    <font style={{ verticalAlign: 'inherit' }}> .</font>
                                  </font>
                                </small>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </span>
                <span className="navbar-text">
                  {/* <div class="dropdown">
                    <button class="" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} fill="currentColor" className="bi bi-cart-plus" viewBox="0 0 16 16">
                    <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z" />
                    <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h6a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                  </svg>
                  
                        </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                      <button class="dropdown-item" href="#">Action</button>
                      <button class="dropdown-item disabled" href="#">Disabled action</button>
                    </div>
                  </div> */}
                  
                </span>
              </div>
            </nav>
          </div>
        </section>

      </div >
        )
    
}
