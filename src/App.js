import './App.css';
import React, { useEffect, useState } from 'react';
import Danhmucsp from './component/Danhmucsp';
import Footer from './component/Footer';
import Menu from './component/Menu';
import Spbanchay from './component/user/Spbanchay';
import Sphamkhuyenmai from './component/Sphamkhuyenmai';
import Store from './component/Store';
import { Name } from './config/ConfigENV';
import axios from 'axios';
function App() {
  console.log('loadApp')
  const [product, setProduct] = useState();

  const fetchData = async () => {
    console.log('call apis')
      const result = await axios.get('http://homemarket.us-3.evennode.com/products', {
        headers: {
          Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImxlZHVjZHVuc2dmZGQ3IiwicmVzdWx0Ijp7InN0b3JlT3duZXJJRCI6eyJuYW1lIjpudWxsLCJwaG90b3MiOm51bGwsImVtYWlsIjpudWxsLCJkZXNjcmlwdGlvbiI6bnVsbCwicGhvbmVOdW1iZXJzIjpudWxsLCJhZGRyZXNzIjpudWxsLCJzdGF0dXMiOiJOT1NUQVRVUyIsIl9pZCI6IjYwNWFiMWJlNjVhZWI1YzkwZmRjNmRjMCIsImNyZWF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguMTkxWiIsInVwZGF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguMTkxWiIsIl9fdiI6MH0sImVtcGxveWVlSUQiOm51bGwsInByb2ZpbGVQaWN0dXJlIjpudWxsLCJnZW5kZXIiOm51bGwsImZ1bGxOYW1lIjoibGUgZHVuZyAxIiwiZW1haWwiOm51bGwsInBob25lIjpudWxsLCJmYWNlYm9va0lEIjpudWxsLCJnb29nbGVJRCI6bnVsbCwic3RhdHVzIjoiVU5MT0NLIiwicm9sZVBlbmRpbmciOiJCQVNJQyIsInJvbGUiOiJBRE1JTiIsIl9pZCI6IjYwNWFiMWJlNjVhZWI1YzkwZmRjNmRjMSIsInVzZXJOYW1lIjoibGVkdWNkdW5zZ2ZkZDciLCJwYXNzV29yZCI6IiQyYiQxMCRhV2VVTnMzdFlhemtDLmVPSVZQMzhlTXFrL1lOWURRbkxHMlpUSkg4d0Y4b1cwWHFJOW1wTyIsImNyZWF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguNDI2WiIsInVwZGF0ZWRBdCI6IjIwMjEtMDMtMjRUMDM6Mjc6NTguNDI2WiIsIl9fdiI6MH0sImlhdCI6MTYxODc1MTAyMiwiZXhwIjoxNjM0MzAzMDIyfQ.aXTDCarp5r6GZfm6aAQi_LKNqx4gW4o1o--LGYgkObI',
        },
      })

    return result.data
  }

  useEffect(() => {
    console.log('call useEffect')
    fetchData().then(data => {
      setProduct(data)
    })
  }, [ ])

if(!product || product.lengh === 0 ){
  return 'page is loading'
}

console.log(product)

  return (
    <div className="App">
      <div className="cha">
        <Menu></Menu>
        <div className="banner">
          <div className="form1">
          </div>
          <h1 className="title_banner">CÀ CHUA GIẢM MẠNH 20%</h1> <br></br>
          <h1 className="title_banner2"><del>20000</del> <span className="giamgiabanner">18000VNĐ</span> </h1>
        </div>
        <Danhmucsp></Danhmucsp>
      </div>
      <Spbanchay product={product} ></Spbanchay>
      <Sphamkhuyenmai></Sphamkhuyenmai>
      <Store></Store>
      <Footer></Footer>
    </div>
  );
}

export default App;
